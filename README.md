### Task Competed

```
.
└── my_dagster_project/
    ├── my_dagster_project/
    │   ├── assets/
    │   │   └── asset_file.py
    │   └── resources/
    │       ├── configs.py
    │       ├── db_io_manager.py
    │       └── plot.py
    ├── my_dagster_project_tests/
    │   └── test_assets.py
    ├── __init__.py
    └── setup.py
```

- I have completed all the tasks asked, addtionaly I have creared plot.py which is help to plot and analyis the enrich_base_data materizled table (dbt_project_marts.revenue_per_customer)
- Please check database structure below:

```
  .
  └── postgres/
  ├── dbt_project_staging/
  │ ├── stg_seed_options_orders
  │ ├── stg_seed_base_data
  │ └── stg_seed_vehicle_line_mapping
  ├── dbt_project/
  │ ├── base-data
  │ ├── options_data
  │ └── vehicle_line_mapping
  └── dbt_project_marts/
  └── enrich_base_data
```

- Lastly, I have created on more assets i.e renvenue_analytics. I am reading the revenue_per_customer in it and ploting it on `http://localhost:8080/horizontal_bar_chart.html`

## Flow

![img.png](jlr.png)
