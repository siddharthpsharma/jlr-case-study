from dagster import Definitions, load_assets_from_modules, ScheduleDefinition, define_asset_job
from dagster._utils import file_relative_path
from .my_dagster_project.assets import assets_file
from dagster_dbt import DbtCliClientResource
from .my_dagster_project.resources import db_io_manager

DBT_PROJECT_DIR = file_relative_path(__file__, "../dbt_project")
DBT_PROFILES_DIR = file_relative_path(__file__, "../dbt_project/config")
DBT_CONFIG = {"project_dir": DBT_PROJECT_DIR, "profiles_dir": DBT_PROFILES_DIR}


defs = Definitions(assets=load_assets_from_modules([assets_file]), resources={
    "dbt": DbtCliClientResource(**DBT_CONFIG),
    "db_io":  db_io_manager.postgres_pandas_io_manager.configured(
        {
            "pwd": "secret",
            "uid": "postgres",
            "server": "host.docker.internal",
            "db": "postgres",
            "port": 5432
        })
}, schedules=[
    ScheduleDefinition(
        job=define_asset_job("all_assets", selection='*'), cron_schedule="@daily"
    ),
],
)
