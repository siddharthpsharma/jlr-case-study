import pandas as pd
import numpy as np
import requests
from logging import Logger
from io import StringIO
from dagster import OpExecutionContext, asset
from dagster_dbt import load_assets_from_dbt_project
from ..resources.configs import config
from dagster._utils import file_relative_path
from ..resources.plot import generate_and_save_plotly_chart, start_local_web_server

DBT_PROJECT_DIR = file_relative_path(__file__, "../../../dbt_project")

# Config params used in configs.py
target_schema, target_table,analytics_table = config.values()


dbt_assets = load_assets_from_dbt_project(
    project_dir=DBT_PROJECT_DIR,
    key_prefix=["dbt_project"]
)


def clean_data(logger: Logger,filename=None, data=None):
    """
    Cleans the input data by handling unknown or empty VINs,
    converting negative quantities and prices to appropriate values,
    and removing duplicates.

    Parameters:
    filename (str): The name of the CSV file to read data from.
    data (list): List of dictionaries representing the data.

    Returns:
    pandas.DataFrame: Cleaned DataFrame.
    """
    try:
        if filename:
            # Read data from CSV file
            df = pd.read_csv(filename)
        elif data:
            # Use provided data as DataFrame
            df = pd.DataFrame(data)
        else:
            raise ValueError("Please provide either a filename or data.")

        # Handle unknown or empty VINs
        df['VIN'] = np.where(df['VIN'].str.strip().eq('') | (df['VIN'] == 'unknown'), 'UNKNOWN_' + df.index.astype(str), df['VIN'])

        # Convert negative Option_Quantities to positive
        df['Option_Quantities'] = df['Option_Quantities'].abs()

        # Convert negative Sales_Price to 0
        df['Sales_Price'] = df['Sales_Price'].apply(lambda x: max(0, x) if pd.notna(x) else 0)

        # Fill missing or negative Option_Quantities with 1
        df['Option_Quantities'] = df['Option_Quantities'].apply(lambda x: max(1, x))

        logger.info('Data cleaning completed successfully.')
        return df
    except Exception as e:
        error_msg = "An error occurred during data cleaning: " + str(e)
        logger.error(error_msg)
        return None


@asset(
    description=f"clean data . Materialized in {target_schema}.{target_table}",
    key_prefix=['dbt_project'],
    group_name="data_ingestion",
    compute_kind="pandas",
    io_manager_key="db_io"
)
def clean_base_data(context: OpExecutionContext) -> pd.DataFrame:
    """
    Asset function.

    Args:
        context (OpExecutionContext): The execution context.

    Returns:
        pd.DataFrame: A DataFrame containing  clean base data
    """
    logger = context.log

    df = clean_data(logger, filename='my_dagster_project/my_dagster_project/resources/base_data.csv')

    return df

@asset(
    description=f"Analytics Materialized in {analytics_table} ",
    deps=['enrich_base_data'],
    key_prefix=["dbt_project"],
    required_resource_keys={"db_io"},
)
def renvenue_analytics(context: OpExecutionContext) -> pd.DataFrame:
    """
    Load revenue analytics data from the database, generate a Plotly chart, and start a local web server to display the chart.

    Args:
        context (OpExecutionContext): The execution context.

    Returns:
        pd.DataFrame: The loaded DataFrame containing revenue analytics data.
    """
    logger = context.log
    loaded_dataframe = context.resources.db_io.read_revenue(analytics_table)
    chart_url = generate_and_save_plotly_chart(loaded_dataframe)
    start_local_web_server(logger, chart_url)
    return loaded_dataframe

