import pandas as pd
import plotly.express as px
import http.server
import socketserver
import logging
import threading
import time

def generate_and_save_plotly_chart(data):
    # Create a horizontal bar chart using Plotly

    # Load the enriched data into a DataFrame
    enriched_data = data
    fig = px.bar(enriched_data, x='model_text', y='total_profit_per_option',
             title='Top Profitable Models - Total Profit per Option',
             labels={'total_profit_per_option': 'Total Profit per Option'})
    chart_url = "horizontal_bar_chart.html"
    fig.write_html(chart_url)
    return chart_url


def start_local_web_server(logger, chart_url, port=8080):
    """
    Start a local web server to serve the Plotly chart HTML file and shuts down when no active connections.

    Args:
        logger: The logger for logging messages.
        chart_url (str): The URL of the HTML chart file to be served.
        port (int): The port number for the local web server.

    Returns:
        None
    """
    class CustomHandler(http.server.SimpleHTTPRequestHandler):
        def end_headers(self):
            self.send_header('Access-Control-Allow-Origin', '*')
            super().end_headers()

    class MyServer(socketserver.TCPServer):
        allow_reuse_address = True

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.active_connections = 0

        def process_request(self, request, client_address):
            self.active_connections += 1
            super().process_request(request, client_address)

        def finish_request(self, request, client_address):
            super().finish_request(request, client_address)
            self.active_connections -= 1

        def shutdown_if_no_connections(self):
            """ Shut down the server if no active connections exist """
            if self.active_connections == 0:
                self.shutdown()

    def serve_server():
        with MyServer(("", port), CustomHandler) as httpd:
            logger.info(f"Serving chart at http://localhost:{port}/{chart_url}")
            logger.info(f"Chart URL: {chart_url}")
            
            # Run the server in a loop, check if there are active connections, and shutdown if none.
            while True:
                httpd.handle_request()  # Handle one request at a time
                httpd.shutdown_if_no_connections()  # Check if there are no active connections

    # Start the server in a separate thread
    server_thread = threading.Thread(target=serve_server)
    server_thread.daemon = True
    server_thread.start()

    # Give the server some time to start up before monitoring
    time.sleep(2)

    # Monitor server and check for active connections
    try:
        while server_thread.is_alive():
            time.sleep(1)  # Periodically check if server thread is alive
    except KeyboardInterrupt:
        logger.info("Server is shutting down...")
