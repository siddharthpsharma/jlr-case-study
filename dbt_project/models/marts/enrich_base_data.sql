{{ config(
  materialized = 'table',
  schema = 'marts',
) }}

WITH production_costs AS (
    SELECT
        b.vin,
        b.option_quantities,
        b.options_code,
        b.option_desc,
        b.model_text,
        b.sales_price,
        o.material_cost,
        CASE
            WHEN b.sales_price <= 0 THEN 0  -- Rule 1: If sales price is zero or negative, production cost is zero
            WHEN o.material_cost IS NOT NULL THEN o.material_cost  -- Rule 2: Use material cost from options data if available
            ELSE NULL  -- Rule 3: Mark for further calculation if no exact match in the options dataset
        END AS production_cost
    FROM {{ ref('stg_seed_base_data') }}  b
    LEFT JOIN 
    (Select * From {{ ref('stg_seed_options_data') }} ) o ON b.model_text = o.model AND b.options_code = o.option_code
),
average_material_cost AS (
    SELECT
        option_code,
        AVG(material_cost) AS avg_material_cost
    FROM {{ ref('stg_seed_options_data') }}
    GROUP BY option_code
)
SELECT 
    p.vin,
    p.option_quantities,
    p.options_code,
    p.option_desc,
    p.model_text,
    p.sales_price,
    p.material_cost,
    amc.avg_material_cost,
    vlm.brand,
    vlm.platform,
    vlm.nameplate_display,
    CASE
        WHEN p.sales_price <= 0 THEN 0  -- Rule 1: If sales price is zero or negative, production cost is zero
        WHEN p.production_cost IS NULL AND amc.avg_material_cost is NOT NULL THEN amc.avg_material_cost
        WHEN p.production_cost IS NULL THEN 0.45 * p.sales_price  -- Rule 4: Assume production cost is 45% of sales price if no matching option code
        ELSE p.production_cost  -- Rule 2: Use calculated production cost if available
    END AS production_cost,
    p.sales_price - COALESCE(p.production_cost,amc.avg_material_cost,0.45 * p.sales_price, 0) AS profit_per_option
FROM production_costs p
LEFT JOIN average_material_cost amc ON p.options_code = amc.option_code
LEFT JOIN  {{ ref('vehicle_line_mapping') }} vlm ON p.model_text=vlm.nameplate_code
ORDER BY vin, sales_price DESC
