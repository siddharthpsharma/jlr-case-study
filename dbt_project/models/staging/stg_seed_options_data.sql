{{ config(
  materialized = 'table',
  schema = 'staging',
) }}

SELECT
  "Model" as model,
  "Option_Code" as option_code,
  "Option_Desc" as option_desc,
  "Material_Cost" as material_Cost
FROM
  {{ ref ("options_data") }}
