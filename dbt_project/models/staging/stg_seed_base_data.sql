{{ config(
  materialized = 'table',
  schema = 'staging',
) }}

SELECT
    "VIN" as vin,
    "Option_Quantities" as option_quantities,
    "Options_Code" as options_code,
    "Option_Desc" as option_desc,
    "Model_Text" as model_text,
    "Sales_Price" as sales_price
    FROM {{ source('dbt_project', 'clean_base_data') }}
