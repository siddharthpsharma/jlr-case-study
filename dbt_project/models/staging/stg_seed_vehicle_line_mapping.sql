-- Reference for dbt modelling
{{ config(
  materialized = 'table',
  schema = 'staging',
) }}

SELECT
   *
FROM
  {{ ref ("vehicle_line_mapping") }}
